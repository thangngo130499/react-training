# PRACTICE-STORYBOOK-HOFMANN

## OVERVIEW

- 1 developer (Thang ngo, mentor: Khanh Nguyen, supporter: Tuan Quang Anh Thai)
- Time line: 14/07/2022 - 20/07/2022
- Design: [Hofmann](https://www.figma.com/file/Qa5whbafWMQp0rbWUDDkkq/Hofmann-UI-Kit)

## TECHNICAL STACK

### Storybook
Storybook is an open source tool for building UI components and pages in isolation. It streamlines UI development, testing, and documentation.

### HTML5

Hypertext Markup Language revision 5 (HTML5) is markup language for the structure and presentation of World Wide Web contents. HTML5 supports the traditional HTML and XHTML-style syntax and other new features in its markup, New APIs, XHTML and error handling.

### CSS3

A cascading style sheet (CSS) is a Web page derived from multiple sources with a defined order of precedence where the definitions of any style element conflict.

### TypeScript
TypeScript is a programming language developed and maintained by Microsoft. It is a strict syntactical superset of JavaScript and adds optional static typing to the language. It is designed for the development of large applications and transpiles to JavaScript.

### React
React is a free and open-source front-end JavaScript library for building user interfaces based on UI components. It is maintained by Meta and a community of individual developers and companies.

## TOOLS
Visual studio code

## Plan

- Implement large components header
    - [x] Implement component logo
    - Implement component navigation
        - [x] Implement component nav mission
        - [ ] Implement component nav services
        - [ ] Implement component nav about
        - [ ] Implement component button take a tour
    - [ ] Implement component title header
    - [ ] Implement component description header
    - [ ] Implement component button explore
- Implement large components section Subscribing Plans
    - Implement component card Freemium
        - [ ] Implement top component Freemium
        - [ ] Implement bottom component Freemium
        - [ ] Implement component button get started for Freemium
    - Implement component card Start up
        - [ ] Implement top component Start up
        - [ ] Implement bottom component Start up
        - [ ] Implement component button get started for Start up
    - Implement component card Business
        - [ ] Implement top component Business
        - [ ] Implement bottom component Business
        - [ ] Implement component button get started for Business
    - Implement component card Enterprise
        - [ ] Implement top component Enterprise
        - [ ] Implement bottom component Enterprise
        - [ ] Implement component button get started for Enterprise
- Implement large components section Features
    - [ ] Implement component title of Features
    - Implement component card responsive layout
        - [ ] Implement component icon of card
        - [ ] Implement component title of card
        - [ ] Implement component description of card
    - Implement component card pixel perfect
        - [ ] Implement component icon of card
        - [ ] Implement component title of card
        - [ ] Implement component description of card
    - Implement component card organized layers
        - [ ] Implement component icon of card
        - [ ] Implement component title of card
        - [ ] Implement component description of card
    - [ ] Implement component img laptop interface hofmann
- Implement large components Stunning Results
    - [ ] Implement component title of Stunning Results
    - [ ] Implement component description of Stunning Results
    - Implement component card Total Conversion
        - [ ] Implement component item percent of card
        - [ ] Implement component title of card
        - [ ] Implement component description of card
    - Implement component card Free Downloads
        - [ ] Implement component item percent of card
        - [ ] Implement component title of card
        - [ ] Implement component description of card
    - Implement component card Unique Visitors
        - [ ] Implement component item percent of card
        - [ ] Implement component title of card
        - [ ] Implement component description of card
- Implement large components section Create An Account
    - [ ] Implement component title of section
    - Implement component form sign up
        - [ ] Implement component input email
        - [ ] Implement component input password
        - [ ] Implement component input confirm password
        - [ ] Implement component nav forgot password
        - [ ] Implement component button sign up
- Implement large components footer
    - Implement component navigation
        - [ ] Implement component nav hotels
        - [ ] Implement component nav services
        - [ ] Implement component nav find more
        - [ ] Implement component nav airline
        - [ ] Implement component nav vacations
        - [ ] Implement component icon pinterest
        - [ ] Implement component icon twitter
        - [ ] Implement component icon facebook
    - [ ] Implement component description of footer
    - [ ] Implement component form search
