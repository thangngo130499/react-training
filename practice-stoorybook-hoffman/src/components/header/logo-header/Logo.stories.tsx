import { ComponentMeta, ComponentStory } from "@storybook/react";
import { Logo } from "./Logo";

const meta: ComponentMeta<typeof Logo> = {
  component: Logo,
  title: "HOFMANN/header/Logo",
}

export default meta


const Template: ComponentStory<typeof Logo> = () => <Logo />

export const Default = Template.bind({})

