import React from "react"
import './logo.css'
import '../../../assets/font.css'

export const Logo = () => {
  return (
    <h1 className="logo-header">
      Hofmann
    </h1>
  )
}
