import { ComponentMeta, ComponentStory } from "@storybook/react";
import { About } from "./About";


const meta: ComponentMeta<typeof About> = {
  component: About,
  title: 'HOFMANN/header/Navigation'
}

export default meta

const Template: ComponentStory<typeof About> = () => <About/>

export const NavAbout = Template.bind({})
