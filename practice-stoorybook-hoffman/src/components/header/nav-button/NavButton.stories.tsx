import { ComponentMeta, ComponentStory } from "@storybook/react";
import { NavButton } from "./NavButton";

const meta: ComponentMeta<typeof NavButton> = {
  title: 'HOFMANN/header/Navigation',
  component:NavButton
}

export default meta

const Template: ComponentStory<typeof NavButton> = () => <NavButton/>

export const Button = Template.bind({})
