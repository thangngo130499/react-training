import './navButton.css'

export const NavButton = () => {
  return(
    <button type="button" className="btn nav_btn">
      take a tour
    </button>
  )
}
