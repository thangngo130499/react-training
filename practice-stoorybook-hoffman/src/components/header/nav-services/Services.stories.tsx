import { ComponentMeta, ComponentStory } from "@storybook/react";
import { Services } from "./Services";

const meta: ComponentMeta<typeof Services> = {
  title: 'HOFMANN/header/Navigation',
  component: Services
}

export default meta

const Template: ComponentStory<typeof Services> = () => <Services/>

export const NavServices = Template.bind({})

