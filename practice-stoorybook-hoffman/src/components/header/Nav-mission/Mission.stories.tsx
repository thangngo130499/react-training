import { ComponentMeta, ComponentStory } from "@storybook/react";
import { Mission } from "./Mission";

const meta: ComponentMeta<typeof Mission> = {
  component: Mission,
  title: "HOFMANN/header/Navigation",
};

export default meta

const Template: ComponentStory<typeof Mission> = () => <Mission />;

export const NavMission = Template.bind({})
